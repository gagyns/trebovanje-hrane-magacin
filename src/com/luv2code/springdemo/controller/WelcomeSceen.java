package com.luv2code.springdemo.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.luv2code.springdemo.entity.Jela;
import com.luv2code.springdemo.entity.JeloZatrebovano;
import com.luv2code.springdemo.entity.Recept;
import com.luv2code.springdemo.services.JelaService;

@Controller
public class WelcomeSceen {
	@Autowired
	private JelaService jelaService;

	private String[] dani = {"Ponedeljak", "Utorak", "Sreda","Cetvrtak", "Petak"};
	private String[] daniZaTrebovanje = {"Ponedeljak", "Utorak", "Sreda","Cetvrtak", "Petak"};
	
	public String[] getIzabraniDani() {
		return daniZaTrebovanje;
	}
	private String[] obrociUdanu = {"Dorucak", "Rucak", "Vecera"};
	private Boolean welcomePageId;
	
	@RequestMapping("/welcome")
	public String welcomeScreen(Model model) {
		welcomePageId = true;
		model.addAttribute("welcome", welcomePageId);
		return "welcome_page";
	}
	@RequestMapping("/trebovanje")
	public String trebovanje(Model model) {
		List<Jela> listaJela = jelaService.getJela();
		model.addAttribute("daniUNedelji", dani);
		model.addAttribute("daniZaTrebovanje", daniZaTrebovanje);
		model.addAttribute("jelaUBazi", listaJela);
		model.addAttribute("obrociUdanu", obrociUdanu);
		return "trebovanje";
	}
	
	@PostMapping ("slanjeIzborDana")
	public String izborDana(HttpServletRequest request, Model model){
		
		String[] daniIzarbani = request.getParameterValues("chckDani");
		String[] testArr = {"12", "dd", "aa"};
		int index = java.util.Arrays.asList(testArr).indexOf("xx");
		daniZaTrebovanje = daniIzarbani;
		System.out.println(">>>>>>>>>>>>>>>Enter<<<<<<<<<<<<< " + index);
		return "redirect:/trebovanje";
	}
	
	@RequestMapping("slanjeTrebovanja")
	public String slanjeTrebovanja(HttpServletRequest request, Model model){
		List <JeloZatrebovano> jelaUNedelji= new ArrayList<>();
		
		for (String dan : dani) {
		    for (String obrok : obrociUdanu) {
		    	// hvatanje izabranog jelalistaJela_PonedeljakDorucak
		    	String idJelaString = request.getParameter("listaJela_" + dan + obrok);
		    	int idJela = Integer.parseInt(idJelaString);
				if(idJela == -1){
					continue;
				}
		    	// servis vraca listu sa jelima iako je jedno jelo pa hvatamo prvi clan
		    	Jela jelo = jelaService.getOneJelo(idJela).get(0);
		    	// Hvatamo iunesenu gramaturu
		    	String gramaturaString = request.getParameter("kolicina_" + dan + obrok);
		    	if( gramaturaString.equals("")) {
					continue;
				}
		    	
		    	int quantity = Integer.parseInt(gramaturaString);
		    	jelaUNedelji.add(new JeloZatrebovano(dan, obrok, jelo, quantity));
		    }
		}
		Map<String, Integer> spisakZaMagacin = new HashMap<>();
		 
		for (JeloZatrebovano jelo : jelaUNedelji) {
			int quantity = jelo.getQuantity();
			Jela trebovanoJelo = jelo.getJelo();
			List<Recept> receptJela = trebovanoJelo.getRecept();
			for (Recept recept: receptJela) {
				String namirnica = recept.getNamirnice().getNamirnica();
				int gramatura = recept.getGramatura();
				int gramaturaZaKolicinu = gramatura * quantity;
				if (spisakZaMagacin.containsKey(namirnica)) {
					int gramaturaIzSpiskaMagacia = spisakZaMagacin.get(namirnica);
					int novaGramaturaZaMagacin = gramaturaIzSpiskaMagacia + gramaturaZaKolicinu;
					spisakZaMagacin.put(namirnica, novaGramaturaZaMagacin);
				} else {
					spisakZaMagacin.put(namirnica, gramaturaZaKolicinu);
				}
			}
		}
		
		System.out.println(">>>>>>>>>>>>> Jela u nedelji" + jelaUNedelji);	
		System.out.println(">>>>>>>>>>>>>>Magacin" + spisakZaMagacin);	
		model.addAttribute("magacinSpisak", spisakZaMagacin);
		return "listaZaMagacin";
	}
}

