<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div>
  <div class="headerNaslov">
    <p>Lista Jela sa sastojcima:</p> 
  </div>
  <c:if test="${showDropDown }">
    <div class="headerListeJela">
      <form action="getOneRecept" method="POST">
        <select name="listaJela" >
          <c:forEach var="theJelo" items="${jela}">
            <option value=${theJelo.id}>${theJelo.jelo}</option>
          </c:forEach>
        </select>
        <button type="submit" class="mdl-button mdl-js-button mdl-js-ripple-effect u_absoluteRnull">
          Prkazi recept
        </button> 

      </form>

    </div>
  </c:if>
    <c:if test="${!showDropDown }">
      <a href="${pageContext.request.contextPath}/lista/listAll">Back</a>
  </c:if>
  <div class="clearfix"></div>
</div>