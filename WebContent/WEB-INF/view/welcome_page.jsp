<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Dobro Dosli</title>
  <jsp:include page="./scriptAndStyles.jsp" />
</head>
<body>
  <jsp:include page="./header.jsp" />
  <div class="welcome-page__chooser-container">
    <div class="demo-card-wide mdl-card mdl-shadow--2dp u_floatL">
		  <div class="mdl-card__title background1">
		    <h2 class="mdl-card__title-text label-welcome">TREBOVANJE</h2>
		  </div>
		  <div class="mdl-card__supporting-text">
        U ovom odeljku se vrsi trebovanje hrane 
        za dane u nedelji.
		  </div>
		  <div class="mdl-card__actions mdl-card--border">
		    <a 
		      href="${pageContext.request.contextPath}/trebovanje"
		      class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
		      ULAZ
		    </a>
		  </div>
		</div>
    
    <div class="demo-card-wide mdl-card mdl-shadow--2dp u_floatR">
      <div class="mdl-card__title background2">
        <h2 class="mdl-card__title-text label-welcome">RECEPTURE</h2>
      </div>
      <div class="mdl-card__supporting-text">
       Pravljenje recepture i unosenje novih namirnica
       Lista namirnica.
      </div>
      <div class="mdl-card__actions mdl-card--border">
        <a 
          href="${pageContext.request.contextPath}/lista/listAll"
          class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
          ULAZ
        </a>
      </div>
    </div>    
    <div class="clearfix">
    </div>
  </div>
</body>
</html>