<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ page import="com.luv2code.springdemo.entity.TestLombok"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <jsp:include page="./scriptAndStyles.jsp" />
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <title>Trebovanje hrane za tekucu nedelju</title>
  <script>
  $(function(){
	  $.get( "${pageContext.request.contextPath}/welcome/getIzabraniDani", function( data ) {
		   var checkBoxovi = $('.daniCheckBox');
		   checkBoxovi.each(function(chboxindex){
			   		  var valueOfCheckBox = $(checkBoxovi[chboxindex]).val();
			   		  if (data.indexOf(valueOfCheckBox) != -1){
			   			  $(checkBoxovi[chboxindex]).trigger("click");
			   		  }
		   })
		});
  })
  </script>
</head>
<body>
  <jsp:include page="./header.jsp" />
  <div class="izbor-dana">
    <div class="u_floatR">
      <form action="slanjeIzborDana" method="POST">
		    <c:forEach var="dani" items="${daniUNedelji}" varStatus="loop">
			      <div class="u_floatL u_margin u_padding">
							<label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="checkbox-${loop.index}">
							  <input type="checkbox" id="checkbox-${loop.index}"
							  name="chckDani" value = "${dani}" class="mdl-checkbox__input daniCheckBox" >
							  <span class="mdl-checkbox__label">${dani} </span>
							  <!-- Test poterivanja metode iz klase -->
                <% TestLombok teraj = new TestLombok(); String output = teraj.getSomething(); %>
							</label>      
			      </div>
		    </c:forEach>
		    <div class="u_floatL u_padding">
		    <!-- Colored mini FAB button -->
					<button
					  type="submit" value="Submit"
					  class="mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab mdl-button--colored">
					  <i class="material-icons">polymer</i>
					</button>
		    </div>
	     <div class="clearfix"></div>
     </form>
     </div>
  </div>
  <div class="trebovanjeMaincontainer">
    <form action="slanjeTrebovanja" method="POST">
	    <c:forEach var="dani" items="${daniZaTrebovanje}">
        <jsp:include page="./trebovanje_dan.jsp">
          <jsp:param name="dani" value="${dani}" />
        </jsp:include>
	    </c:forEach>
	    <div class="btn_posalji_u_magacin">
	     <button class="mdl-button mdl-js-button mdl-button--raised" type="submit" value="Submit">
	       <i class="material-icons">receipt</i>
	       Posalji u magacin
		     <!-- <input type="submit" value="Submit">   -->
       </button>
	    </div>
    </form>
  </div>
</body>
</html>