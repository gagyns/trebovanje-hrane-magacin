package com.luv2code.springdemo.entity;

public class JeloZatrebovano {
	private String danUnedelji;
	private String obrokUdanu;
	private Jela jelo;
	private int quantity;
	
	public JeloZatrebovano(String danUnedelji, String obrokUdanu, Jela jelo, int quantity) {
		this.danUnedelji = danUnedelji;
		this.obrokUdanu = obrokUdanu;
		this.jelo = jelo;
		this.quantity = quantity;
	}
	public Jela getJelo() {
		return jelo;
	}
	public void setJelo(Jela jelo) {
		this.jelo = jelo;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	@Override
	public String toString() {
		return "JeloZatrebovano [danUnedelji=" + danUnedelji + ", obrokUdanu=" + obrokUdanu + ", jelo=" + jelo
				+ ", quantity=" + quantity + "]";
	}
	
}
