<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="danUNedeljiContainer mdl-data-table mdl-shadow--2dp">
 <h6 class="u_marginTop5px"><%=request.getParameter("dani") %></h6>
 <ul>
  <c:forEach var="obrociUdanu" items="${obrociUdanu}">
  <li>
   ${obrociUdanu}<br>
    <label>Vrsta jela:</label>
      <select
        class="selectJeloTrebovanje"
        name="listaJela_<%=request.getParameter("dani") %>${obrociUdanu}" >
        <option value="-1"> - </option>
        <c:forEach var="theJelo" items="${jelaUBazi}">
          <option value=${theJelo.id}>${theJelo.jelo}</option>
        </c:forEach>
    </select>
    <label>Kolicina:</label>
    <input 
      class="inputGramiTrebovanje"
      type="text"
      name="kolicina_<%=request.getParameter("dani") %>${obrociUdanu}" />
    <br>
    <br>
  </li>
 </c:forEach>
 </ul>
</div>