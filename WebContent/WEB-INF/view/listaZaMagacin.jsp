<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <jsp:include page="./scriptAndStyles.jsp" />
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <title>Lista za magacin</title>
</head>
<body>
    <jsp:include page="./header.jsp" />
<table class="table-trebovanje mdl-data-table mdl-js-data-table  mdl-shadow--2dp">
  <thead>
    <tr>
      <th class="mdl-data-table__cell--non-numeric">Lista za magacin</th>
    </tr>
    <tr>
      <th class="mdl-data-table__cell--non-numeric">Namirnica</th>
      <th>Quantity</th>
    </tr>
  </thead>
  <tbody>
   <c:forEach items="${magacinSpisak}" var="spisak">
    <tr>
	      <td class="mdl-data-table__cell--non-numeric">${spisak.key}</td>
	      <td>${spisak.value} grama</td>
    </tr>
   </c:forEach>
  </tbody>
</table>
    
</body>
</html>