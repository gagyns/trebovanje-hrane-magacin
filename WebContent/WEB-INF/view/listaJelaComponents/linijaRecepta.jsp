
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="linijaRecepta">
  <div class="mdl-textfield mdl-js-textfield">
    <input class="mdl-textfield__input" type="number" name="inputGrama" value= "${param.gramatura}" id="sample1">
    <label class="mdl-textfield__label" for="sample1">Gramatura:</label>
  </div>
  <select name="listaNamirnica" >
    <option value=0>Odaber namirnicu</option>
    <c:forEach var="theNamirnice" items="${namirnice}">
      <option value=${theNamirnice.id}  ${theNamirnice.namirnica == param.namirnice ? 'selected="selected"': ''}>${theNamirnice.namirnica}</option>
    </c:forEach>
  </select>
</div>
<br/>
