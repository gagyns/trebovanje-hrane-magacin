package com.luv2code.springdemo.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="jela")
public class Jela {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	@Transient
	private int editID;
	
	@Column(name="jelo")
	private String jelo;
	
	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	@JoinColumn(name="jela_id")
	private List<Recept> recepti;
	
	public void addToRecepti(Recept recept){
		if(recepti == null){
			recepti = new ArrayList<Recept>();
		}
//		recept.setJela(this);
		recepti.add(recept);
	}
	
	public Jela() {}

	public Jela(String jelo) {
		this.jelo = jelo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getEditID() {
		return editID;
	}

	public void setEditID(int editID) {
		this.editID = editID;
	}

	public String getJelo() {
		return jelo;
	}

	public void setJelo(String jelo) {
		this.jelo = jelo;
	}
	
	

	public List<Recept> getRecept() {
		return recepti;
	}

	public void setRecept(List<Recept> theRecepti) {
		this.recepti = theRecepti;
	}



	@Override
	public String toString() {
		return "ID= " + id + " jelo=" + jelo + ", recepti=" + recepti + "]";
	}


}
