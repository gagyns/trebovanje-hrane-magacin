<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Editovanje Recepta</title>
<jsp:include page="./scriptAndStyles.jsp" />
</head>
<body>
<jsp:include page="./header.jsp" />
  <div class="mdl-shadow--2dp u_margin editReceptcontainer">
	  <form action="editRecept" method="POST">
	    <input type="hidden" name="id" value="${idJela}" />
	    <p>Ime jela:</p>
		  <input class="u_width50pct mdl-textfield__input" type="text" name="nazivJela" value="${imeJela}"/>
	    <hr>
	    <div class="inputRecepturaContainer">
	    <c:forEach var="theNamirnice" items="${receptura}">
		      <jsp:include page="./listaJelaComponents/linijaRecepta.jsp">
		        <jsp:param name="gramatura" value="${theNamirnice.gramatura}"/>
		        <jsp:param name="namirnice" value="${theNamirnice.namirnice}"/>
		      </jsp:include>
	    </c:forEach>
	      <!-- LINIJA RECEPTA -->
	    </div>
	    <input type="submit" value="Submit">
	  </form>
  </div>
</body>
</html>