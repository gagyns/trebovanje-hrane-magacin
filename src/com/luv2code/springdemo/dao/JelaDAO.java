package com.luv2code.springdemo.dao;

import java.util.List;

import com.luv2code.springdemo.entity.Jela;
import com.luv2code.springdemo.entity.Namirnice;

public interface JelaDAO {
	public List<Jela> getJela();

	public List<Jela> getJelo(int i);

	public List<Namirnice> getNamirnice();

	public void saveNamirnica(Namirnice jednaNamirnica);

	public Namirnice getNamirnica(int i);

	public void saveJelo(Jela novoJelo);

	public void deleteJelo(int jeloID);

	public void updateJelo(Jela editJelo);
}
