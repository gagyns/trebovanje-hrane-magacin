<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="listaJela">
  <c:forEach var="theJelo" items="${jela}">
  <c:url var= "deleteLink" value="/lista/deleteJelo">
     <c:param name="jeloID" value="${theJelo.id}"></c:param>
  </c:url>
  <c:url var= "editLink" value="/lista/editJelo">
     <c:param name="jeloID" value="${theJelo.id}"></c:param>
  </c:url>
  <div class="u_floatL u_margin">
    <table class="mdl-data-table mdl-shadow--2dp">
     <thead>
	      <tr>
	      <th >${theJelo.jelo}</th>
	   </tr>
	    <tr>
	      <th class="mdl-data-table__cell--non-numeric">Namirnica</th>
	      <th>Gramaza</th>
	    </tr>
	  </thead>
    <tbody>
      <c:forEach var="theRecept" items="${theJelo.getRecept()}">
		    <tr>
		      <td>${theRecept.namirnice}</td>
		      <td class="mdl-data-table__cell--non-numeric">
		        ${theRecept.gramatura} grama
		      </td>
		    </tr>
      </c:forEach>
        <tr>
          <td>
            <a href="${deleteLink}"><button>Delete</button></a>
          </td>
          <td class="mdl-data-table__cell--non-numeric">
            <a href="${editLink}"><button>Edit</button></a>
          </td>
        </tr>
    </tbody>
    </table>
  </div>

  </c:forEach>
  <div class="clearfix"></div>
</div>

