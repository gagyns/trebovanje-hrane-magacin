package com.luv2code.springdemo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.luv2code.springdemo.entity.Jela;
import com.luv2code.springdemo.entity.ResponseClass;
import com.luv2code.springdemo.services.JelaService;

@RestController
@RequestMapping("/welcome")
public class magacinRest {
	
	@Autowired
	private JelaService jelaService;
	@Autowired
	private WelcomeSceen wecomeController;

	@GetMapping("/rest")
	public ResponseEntity <ResponseClass>  returnHello(){
		List<Jela> listaJela = jelaService.getJela();
		ResponseClass response = new ResponseClass("neki string", 22222, listaJela);
		System.out.println(">>>>>>>>>>>>>>>"+response);
        return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@GetMapping("/getIzabraniDani")
	public String[] GetIzabraniDani(){
		return wecomeController.getIzabraniDani();
	}
	
	@GetMapping("/restList")
	@ResponseBody
	public List<Jela> listaJela(){
		List<Jela> listaJela = jelaService.getJela();
        return listaJela;
	}
	@PostMapping("/restPost")
	public String returnHelloParamPost(@RequestParam String User){
		return "Hello World" + User;
	}
	
	@GetMapping("/restSend")
	public String returnHelloParam(@RequestParam String User){
		return "Hello World" + User;
	}
}
