package com.luv2code.springdemo.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.luv2code.springdemo.entity.Jela;
import com.luv2code.springdemo.entity.Namirnice;
//import com.luv2code.springdemo.entity.Recept;

@Repository
public class JelaDAOImpl implements JelaDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<Jela> getJela() {
		Session curentSession = sessionFactory.getCurrentSession();
		Query<Jela> theQuery = curentSession.createQuery("from Jela", Jela.class);
		List<Jela> jela = theQuery.getResultList();
		return jela;
	}

	@Override
	public List<Jela> getJelo(int i) {
		Session curentSession = sessionFactory.getCurrentSession();
		@SuppressWarnings("unchecked")
		Query<Jela> theQuery = curentSession.createQuery("from Jela where id =:jeloId");
		theQuery.setParameter("jeloId", i);
		List<Jela> jela = theQuery.getResultList();
		return jela;
	}

	@Override
	public List<Namirnice> getNamirnice() {
		Session curentSession = sessionFactory.getCurrentSession();

		Query<Namirnice> theQuery = curentSession.createQuery("from Namirnice", Namirnice.class);
		List<Namirnice> namirnice = theQuery.getResultList();
		return namirnice;
	}

	@Override
	public void saveNamirnica(Namirnice jednaNamirnica) {
		Session curentSession = sessionFactory.getCurrentSession();
		
		curentSession.save(jednaNamirnica);
		
		
	}

	@Override
	public Namirnice getNamirnica(int i) {
		Session curentSession = sessionFactory.getCurrentSession();
		@SuppressWarnings("unchecked")
		Query<Namirnice> theQuery = curentSession.createQuery("from Namirnice where id=:namirniceId");
		theQuery.setParameter("namirniceId", i);
		Namirnice jednaNamirnica = theQuery.getSingleResult();
		return jednaNamirnica;
	}

	@Override
	public void saveJelo(Jela novoJelo) {
		Session curentSession = sessionFactory.getCurrentSession();
		curentSession.save(novoJelo);		
	}

	@Override
	public void deleteJelo(int jeloID) {
		System.out.println(">>>>>> DAO >>>> " + jeloID);
		Session curentSession = sessionFactory.getCurrentSession();
		@SuppressWarnings("rawtypes")
		Query theQuery = curentSession.createQuery("delete from Jela where id=:jeloID");
		theQuery.setParameter("jeloID", jeloID);
		theQuery.executeUpdate();
	}

	@Override
	public void updateJelo(Jela editJelo) {
		Session curentSession = sessionFactory.getCurrentSession();
		int idOfEditJelo = editJelo.getEditID();
		System.out.println(" >>>>>>>>>>>>>> EDIT INDEX " + idOfEditJelo );
		
		
		Jela jeloFromDB = curentSession.get(Jela.class, idOfEditJelo);
		
		System.out.println(" editovano jelo " + editJelo );

		jeloFromDB.setJelo(editJelo.getJelo());
		jeloFromDB.setRecept(editJelo.getRecept());
		System.out.println(" >>>>>>>>>>>>>> usao u update " + jeloFromDB );
	
		curentSession.update(jeloFromDB);
	}
	
	

}