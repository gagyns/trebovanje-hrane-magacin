package com.luv2code.springdemo.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class TestLombok {
	private int id;
	private String someString;
	
	public String getSomething() {
		System.out.println(" !!!!! >>>>>>>> radiii pozivanje JSP");
		return "RADIIIII";
	}
}
