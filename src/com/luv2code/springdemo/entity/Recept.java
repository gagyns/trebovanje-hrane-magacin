package com.luv2code.springdemo.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="recepti")
public class Recept {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
//	@ManyToOne(cascade=CascadeType.ALL)
//	@JoinColumn(name= "jela_id")
//	@Column(name="jela_id")
//	private int jelaID;
	
	@Column(name="gramatura")
	private int gramatura;
	
	@ManyToOne(cascade={CascadeType.DETACH, CascadeType.MERGE,
			CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinColumn(name="namirnice_id")
	private Namirnice namirnice;
	
	public Recept() {}
	
	public Recept(Jela jela, int gramatura, Namirnice namirnice) {
//		this.jela = jela;
		this.gramatura = gramatura;
		this.namirnice = namirnice;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

//	public Jela getJela() {
//		return jela;
//	}
//
//	public void setJela(Jela theJela) {
//		this.jela = theJela;
//	}

	public int getGramatura() {
		return gramatura;
	}

	public void setGramatura(int gramatura) {
		this.gramatura = gramatura;
	}

	public Namirnice getNamirnice() {
		return namirnice;
	}

	public void setNamirnice(Namirnice namirnice) {
		this.namirnice = namirnice;
	}

	@Override
	public String toString() {
		return gramatura + " grama " + namirnice;
	}





	
	
	
}
