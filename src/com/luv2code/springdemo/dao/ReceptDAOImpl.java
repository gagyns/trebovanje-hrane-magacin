package com.luv2code.springdemo.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ReceptDAOImpl implements ReceptDAO {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void cleanReceptList() {
		Session curentSession = sessionFactory.getCurrentSession();
		@SuppressWarnings("rawtypes")
		Query theQuery = curentSession.createQuery("delete from Recept where jelaID=null");
		theQuery.executeUpdate();
	}

}
