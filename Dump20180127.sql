-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: 1a_magacin
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `jela`
--

DROP TABLE IF EXISTS `jela`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jela` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jelo` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jela`
--

LOCK TABLES `jela` WRITE;
/*!40000 ALTER TABLE `jela` DISABLE KEYS */;
INSERT INTO `jela` VALUES (11,'Kajgana'),(17,'Pizza');
/*!40000 ALTER TABLE `jela` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `namirnice`
--

DROP TABLE IF EXISTS `namirnice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `namirnice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namirnica` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `namirnice`
--

LOCK TABLES `namirnice` WRITE;
/*!40000 ALTER TABLE `namirnice` DISABLE KEYS */;
INSERT INTO `namirnice` VALUES (1,'jaja'),(2,'sol'),(3,'tikvica'),(4,'ulje'),(5,'sir'),(28,'Karfiol'),(29,'Karfiol'),(30,'Pivo'),(31,'Povrce');
/*!40000 ALTER TABLE `namirnice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recepti`
--

DROP TABLE IF EXISTS `recepti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recepti` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jela_id` int(11) DEFAULT NULL,
  `gramatura` int(11) DEFAULT NULL,
  `namirnice_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `jela_id_idx` (`jela_id`),
  KEY `namirnice_id_idx` (`namirnice_id`),
  CONSTRAINT `namirnice_id` FOREIGN KEY (`namirnice_id`) REFERENCES `namirnice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=159 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recepti`
--

LOCK TABLES `recepti` WRITE;
/*!40000 ALTER TABLE `recepti` DISABLE KEYS */;
INSERT INTO `recepti` VALUES (86,11,150,1),(87,11,5,2),(88,11,10,4),(156,17,15,2),(157,17,55,5),(158,17,50,29);
/*!40000 ALTER TABLE `recepti` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-27 23:59:30
