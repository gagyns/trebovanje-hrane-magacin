package com.luv2code.springdemo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.luv2code.springdemo.dao.ReceptDAO;

@Service
public class ReceptServiceImpl implements ReceptService {
	
	@Autowired
	ReceptDAO receptDAO;

	@Override
	@Transactional
	public void cleanReceptList() {
		receptDAO.cleanReceptList();
	}

}
