package com.luv2code.springdemo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.luv2code.springdemo.entity.Jela;
import com.luv2code.springdemo.entity.Namirnice;
import com.luv2code.springdemo.entity.Recept;
import com.luv2code.springdemo.services.JelaService;
import com.luv2code.springdemo.services.ReceptService;

@Controller
@RequestMapping("/lista")
public class JelaNamirnice {
	
	@Autowired
	private JelaService jelaService;
	
	@Autowired
	private ReceptService receptService;
	
	@GetMapping("/listAll")
	public String listAll(Model theModel ) {
		List<Jela> listaJela = jelaService.getJela();
		List<Namirnice> namirnice = jelaService.getNamirnice();
		theModel.addAttribute("jela", listaJela);
		theModel.addAttribute("showDropDown", true);
		theModel.addAttribute("namirnice", namirnice);
		theModel.addAttribute("namirniceZaJS", namirnice.toString() );
		return "listaJela";
	}
	
	@PostMapping("/getOneRecept")
	public String listOneJelo(@RequestParam("listaJela")int index,  Model theModel ) {
		List<Jela> listaJela = jelaService.getOneJelo(index);
		theModel.addAttribute("jela", listaJela);
		theModel.addAttribute("showDropDown", false);
		return "listaJela";
	}
	
	@PostMapping("/postNewNamirnica")
	public String addNewJelo(@RequestParam("nazivNamirnice")String theNamirnica) {
		Namirnice jednaNamirnica = new Namirnice(theNamirnica);
		jelaService.saveNamirnica(jednaNamirnica);
		return "redirect:/lista/listAll";
	}
	
	@PostMapping("/postNewRecept")
	public String addNewrecept(
			@RequestParam("nazivJela")String theNamirnica,
			@RequestParam("inputGrama")List<Integer> listaGrama,
			@RequestParam("listaNamirnica")List<Integer> listaNamirnica
	) {
		Jela novoJelo = new Jela(theNamirnica);
		for(int ReqListInd=0; ReqListInd < listaGrama.size(); ReqListInd++){
			// ---- ulaz u for petlju -----
			int grami = listaGrama.get(ReqListInd);
			int indexNamirnice = listaNamirnica.get(ReqListInd);
			if(grami == 0 || indexNamirnice == 0){
				continue;
			}
			Namirnice izabranaNamirnica = jelaService.getNamirnica(indexNamirnice);
			Recept noviRecept = new Recept(novoJelo, grami, izabranaNamirnica);
			novoJelo.addToRecepti(noviRecept);
			// ----- kraj petlje -------
		}
		jelaService.saveJelo(novoJelo);
		return "redirect:/lista/listAll";
	}
	
	@GetMapping("/deleteJelo")
	public String deleteJelo(@RequestParam("jeloID") int jeloID){
		System.out.println(" <<<<<< --- >>>>>>>>>> Jelo id : " + jeloID );
		jelaService.deleteJelo(jeloID);
		return "redirect:/lista/listAll";
	}
	
	@GetMapping("/editJelo")
	public String editJelo(@RequestParam("jeloID") int jeloID, Model theModel){
		List<Jela> listaJela = jelaService.getOneJelo(jeloID);
		List<Namirnice> namirnice = jelaService.getNamirnice();
		Jela jednoJelo = listaJela.get(0);
		int idJela = jednoJelo.getId();
		String ImeJela = jednoJelo.getJelo();
		List<Recept> receptura = jednoJelo.getRecept();
		theModel.addAttribute("idJela", idJela);
		theModel.addAttribute("imeJela", ImeJela);
		theModel.addAttribute("receptura", receptura);
		theModel.addAttribute("namirnice", namirnice);
		return "editJelo";
	}
	@PostMapping("/editRecept")
	public String editRecept(
			@RequestParam("id") int JeloID,
			@RequestParam("nazivJela")String theNamirnica,
			@RequestParam("inputGrama")List<Integer> listaGrama,
			@RequestParam("listaNamirnica")List<Integer> listaNamirnica
	) {
		Jela editJelo = new Jela(theNamirnica);
		for(int ReqListInd=0; ReqListInd < listaGrama.size(); ReqListInd++){
			// ---- ulaz u for petlju -----
			int grami = listaGrama.get(ReqListInd);
			int indexNamirnice = listaNamirnica.get(ReqListInd);
			if(grami == 0 || indexNamirnice == 0){
				continue;
			}
			Namirnice izabranaNamirnica = jelaService.getNamirnica(indexNamirnice);
			Recept noviRecept = new Recept(editJelo, grami, izabranaNamirnica);
			editJelo.addToRecepti(noviRecept);
			// ----- kraj petlje -------
		}
		editJelo.setEditID(JeloID);
		jelaService.updateJelo(editJelo);
//		receptService.cleanReceptList();
		return "redirect:/lista/listAll";
	}
}
