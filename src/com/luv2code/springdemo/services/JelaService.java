package com.luv2code.springdemo.services;

import java.util.List;

import com.luv2code.springdemo.entity.Jela;
import com.luv2code.springdemo.entity.Namirnice;

public interface JelaService {
	public List<Jela>getJela();

	public List<Jela> getOneJelo(int i);

	public List<Namirnice> getNamirnice();

	public void saveNamirnica(Namirnice jednaNamirnica);

	public Namirnice getNamirnica(int i);

	public void saveJelo(Jela novoJelo);

	public void deleteJelo(int jeloID);

	public void updateJelo(Jela editJelo);
}
