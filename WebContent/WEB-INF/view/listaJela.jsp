<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<jsp:include page="./scriptAndStyles.jsp" />
<title>List Customers</title>
</head>
<body>
  <jsp:include page="./header.jsp" />
<div class="mainContainer">
	<div class="leftPanel">
    <!-- Heder liste jela -->
    <jsp:include page="./listaJelaComponents/hederListeJela.jsp" />		
		<hr/>
		<!-- LISTA RECEPATA -->
		<jsp:include page="./listaJelaComponents/listaRecepata.jsp" />
	</div>
  <div class="rightPanel">
    <div>
	    <form action="postNewNamirnica" method="POST">
        <div class="mdl-textfield mdl-js-textfield">
          <input class="mdl-textfield__input" type="text" id="sample1" name="nazivNamirnice" value="${theJelo.jelo}">
          <label class="mdl-textfield__label" for="sample1">Dodaj namirnicu:</label>
        </div>
        <button type="submit" class="mdl-button mdl-js-button mdl-js-ripple-effect u_absoluteRnull">
          Unesi
        </button> 
	   </form>
    </div>
    <div>
    <br/>
      <form action="postNewRecept" method="POST">
        <div class="mdl-textfield mdl-js-textfield">
			    <input class="mdl-textfield__input" type="text" id="sample1" name="nazivJela">
			    <label class="mdl-textfield__label" for="sample1">Ime jela:</label>
			  </div>
        <hr>
        <div class="inputRecepturaContainer">
			    <!-- LINIJA RECEPTA -->
			    <jsp:include page="./listaJelaComponents/linijaRecepta.jsp" />
			    
          <!-- LINIJA RECEPTA -->
          <jsp:include page="./listaJelaComponents/linijaRecepta.jsp" />
          
          <!-- LINIJA RECEPTA -->
          <jsp:include page="./listaJelaComponents/linijaRecepta.jsp" />
          
        </div>
        <button type="submit" class="mdl-button mdl-js-button mdl-js-ripple-effect u_absoluteRnull">
          POSALJI RECEPT
        </button> 
		  </form>
		  <button 
		    class="mdl-button mdl-js-button mdl-js-ripple-effect u_absoluteRnull" 
		    id="addLine">Dodaj liniju recepta</button>
		  <script>
		  $('#addLine').click(function(){addLineOfRecepie()})

		  function addLineOfRecepie(){
			  var namirniceArr = '${namirniceZaJS}'.split(',');


        var chooser = $('<select>', {
          name: 'listaNamirnica',
          html: '<option value=0>Odaberi namirnicu</option>'
        });
        
        namirniceArr.forEach(function(namirnica, index) {
          var option = $('<option>', {
            value: index + 1,
            html: namirnica
          });
          chooser.append(option);
        })
      
        var linijaReceptaContainer = $('<div>', {
          class: 'linijaRecepta',
          html: ['<div class="mdl-textfield mdl-js-textfield">'
                 + '<input class="mdl-textfield__input" type="number" name="inputGrama" id="sample1">'
                 + '<label class="mdl-textfield__label" for="sample1">Gramatura:</label>'
                 + '</div>'
          , chooser]
        });

			  $('.inputRecepturaContainer').append([linijaReceptaContainer, '<br>'])
		  }
		  </script>
    </div>
  </div>
  <div class="clearfix"></div>
</div>
</body>

</html>