package com.luv2code.springdemo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="namirnice")
public class Namirnice {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="namirnica")
	private String namirnica;
	
	public Namirnice() {}
	
	public Namirnice(String namirnica) {
		this.namirnica = namirnica;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNamirnica() {
		return namirnica;
	}

	public void setNamirnica(String namirnica) {
		this.namirnica = namirnica;
	}

	@Override
	public String toString() {
		return namirnica;
	}
	
	
}
