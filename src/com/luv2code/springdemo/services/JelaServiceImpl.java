package com.luv2code.springdemo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.luv2code.springdemo.dao.JelaDAO;
import com.luv2code.springdemo.entity.Jela;
import com.luv2code.springdemo.entity.Namirnice;

@Service
public class JelaServiceImpl implements JelaService {
	
	@Autowired 
	private JelaDAO jelaDAO;

	@Override
	@Transactional
	public List<Jela> getJela() {
		return jelaDAO.getJela();
	}

	@Override
	@Transactional
	public List<Jela> getOneJelo(int i) {
		return jelaDAO.getJelo(i);
	}

	@Override
	@Transactional
	public List<Namirnice> getNamirnice() {
		return jelaDAO.getNamirnice();
	}

	@Override
	@Transactional
	public void saveNamirnica(Namirnice jednaNamirnica) {
		jelaDAO.saveNamirnica(jednaNamirnica);
		
	}

	@Override
	@Transactional
	public Namirnice getNamirnica(int i) {
		return jelaDAO.getNamirnica(i);
	}

	@Override
	@Transactional
	public void saveJelo(Jela novoJelo) {
		jelaDAO.saveJelo(novoJelo);
		
	}

	@Override
	@Transactional
	public void deleteJelo(int jeloID) {
		System.out.println(">>>> service JeloID >>>" +jeloID);
		jelaDAO.deleteJelo(jeloID);
		
	}

	@Override
	@Transactional
	public void updateJelo(Jela editJelo) {
		jelaDAO.updateJelo(editJelo);
	}

}
