package com.luv2code.springdemo.entity;

import java.util.List;
import com.luv2code.springdemo.entity.Jela;

public class ResponseClass {
	private String string;
	private int number;
	private List<Jela> listaJela;
	public ResponseClass() {

	}

	public ResponseClass(String string, int number, List<Jela> listaJela) {
		this.string = string;
		this.number = number;
		this.listaJela = listaJela;
	}

	public List<Jela> getListaJela() {
		return listaJela;
	}

	public void setListaJela(List<Jela> listaJela) {
		this.listaJela = listaJela;
	}

	public String getString() {
		return string;
	}
	public void setString(String string) {
		this.string = string;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}

	
}
