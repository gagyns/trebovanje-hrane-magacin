 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
  
 <div class="header-container ">
   <div class="header-icon u_floatL">
   <c:choose>
     <c:when test="${welcome}">
	    <i class="fa fa-cutlery u_floatL escajg-icon" aria-hidden="true"></i>
     </c:when>
     <c:otherwise>
	    <a href="${pageContext.request.contextPath}/welcome">
	      <button 
	        class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect u_psAbs u_topLeftFive ">
	        <i class="material-icons">home</i>
	      </button>
	    </a>     
     </c:otherwise>
     </c:choose>
   </div>
   <div class="header-title u_floatR">
   APLIKACIJA ZA TREBOVANJE HRANE</div>
 </div>